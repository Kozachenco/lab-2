import java.io.*;
import java.util.*;
import java.nio.file.*;
import java.nio.charset.Charset;

public class Lab2 {
  // Считывает из in.txt по три точки с каждой строки
  // и записывает в out.txt площадь между каждой тройкой точек
  public static void main(String[] args) {
    Scanner in;
    Path out = Paths.get("out.txt"); // Создаем выходной файл
  
    try {
      in = new Scanner(new File("in.txt")); // Пытаемся открыть входной файл
    } catch (FileNotFoundException e) { // Если не получается выводим сообщение об ошибке
      System.out.println("Не получилось открыть файл in.txt");
      return; // и выходим
    }
    
    // Создаем список строк, которые будем записывать в выходной файл
    ArrayList<String> stroki = new ArrayList<>();

    while (in.hasNextLine()) {
      String stroka = in.nextLine();
      Point3d[] pts = toch(stroka); // Считываем из строки три точки
      double s = ploshad(pts); // Находим площадь между этими точками
      stroki.add(Double.toString(s)); // Добавляем в список
    }

    try {
      Files.write(out, stroki, Charset.forName("UTF-8"));
    } catch(IOException e) {
      System.out.println("Не получилось записать out.txt");
    }
  }

  // Считывает из строки три точки
  private static Point3d[] toch(String stroka) {
    // Разбиваем строку по запятой с пробелом
    String[] pts = stroka.split(",\\s+");
    double[] coordinates;
    Point3d[] res = new Point3d[3]; // Массив результатов
    for (int i = 0; i < pts.length; i++) {
      coordinates = coordinates(pts[i]); // считываем в массив из строки координаты точки
      res[i] = new Point3d(coordinates[0], coordinates[1], coordinates[2]); // создаем новую точку
    }
    return res;
  }

  // Считывает из строки три координаты и возвращает точку
  private static double[] coordinates(String tochka) {
    // Разбиваем строку по пробелам
    String[] coordinatesStr = tochka.split("\\s+");
    double[] coordinates = new double[3]; // Создаем массив координат, который вернем
    for (int j = 0; j < coordinatesStr.length; j++) { // Проходимся по разбитым строкам
      coordinates[j] = Double.parseDouble(coordinatesStr[j]); // Каждую координату приводим из строки в число
    }
    return coordinates; // Возвращаем сформированный массив координат
  }

  // Находит площадь между тремя точками
  private static double ploshad(Point3d[] pts) {
    double a = pts[0].distanceTo(pts[1]); // Находим длины сторон треугольника
    double b = pts[1].distanceTo(pts[2]);
    double c = pts[2].distanceTo(pts[0]);
    double p = (a + b + c) / 2; // Находим полупериметр
    double s = Math.sqrt(p * (p - a) * (p - b) * (p - c)); // Вычисляем площадь по формуле Герона
    return s;
  }
}
